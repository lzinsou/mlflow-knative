ARG PYTHON_VERSION=3.11

FROM python:${PYTHON_VERSION}-slim-bullseye

RUN apt update -y && apt upgrade -y

# Install `poetry`
ENV \
  POETRY_HOME=/etc/poetry \
  POETRY_VERSION=1.5.1
RUN apt install -y --no-install-recommends \
    curl \
    build-essential \
    git \
  && curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="$POETRY_HOME/bin:$PATH"

WORKDIR /mlflow

# Install all dependencies
COPY pyproject.toml poetry.lock ./
RUN poetry install --sync --no-root

# Copy project
COPY . .
RUN poetry install --only-root

# Install Docker CLI
COPY --from=docker:23.0.6-dind /usr/local/bin/docker /usr/local/bin/

# Do not buffer stdout & stderr output, enable dev mode
ENV \
  PYTHONDEVMODE=1 \
  PYTHONUNBUFFERED=1

# Ensure `poetry` runs commands (within virtualenv)
ENTRYPOINT ["poetry", "run"]
