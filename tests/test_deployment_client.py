"""MLflow Knative deployment client integration tests."""
from http import HTTPStatus
from unittest.mock import MagicMock, patch

import kubernetes
import mlflow
import pytest
import requests
from mlflow.deployments import get_deploy_client

from mlflow_knative import knative
from mlflow_knative.deployment_client import (
    DISTRIBUTION_NAME,
    MANAGED_BY_LABEL,
    MLFLOW_MODEL_FLAVOR_PYFUNC,
    RUN_ID_ANNOTATION,
    run_local,
    target_help,
)

from .conftest import ARTIFACT_PATH


@pytest.fixture(scope="session")
def client(context):
    """Obtain the Knative deployment client from MLflow."""
    return get_deploy_client(f"knative:/{context}")


@pytest.fixture
def deployment(client, image_repository, first_run):
    """Default test deployment."""
    response = client.create_deployment(
        name="test",
        model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
        config={
            "image_repository": image_repository,
            "image_tag": first_run.info.run_id,
        },
    )
    yield response
    try:
        client.delete_deployment(response["name"])
    except mlflow.exceptions.MlflowException:
        # Ignore if the deployment is already deleted, as some tests might delete
        # the deployment as part of the test logic.
        pass


def test_target_help():
    """When requesting target help, a help string is returned."""
    assert "Knative" in target_help()


@pytest.mark.parametrize("config", (None, {}, {"host": "0.0.0.0", "port": 5000}))
def test_run_local(run, config):
    """When running a model locally, ensure config is passed to MLflow local server."""
    model_uri = f"runs:/{run.info.run_id}/{ARTIFACT_PATH}"
    with patch(
        "mlflow_knative.deployment_client.mlflow.pyfunc.load_model"
    ) as mock_load_model:
        mock_load_model.return_value = MagicMock()
        run_local("test", model_uri, config=config)

    config = config if config is not None else {}
    mock_load_model.assert_called_once_with(model_uri)
    mock_load_model.return_value.serve.assert_called_once_with(
        enable_mlserver=True,
        host=config.get("host", "localhost"),
        port=config.get("port", 8080),
    )


def test_run_local_unsupported_flavor_error(run):
    """When running a model locally with an unsupport flavor, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        run_local("test", f"runs:/{run.info.run_id}/{ARTIFACT_PATH}", flavor="bad")

    assert "flavor" in str(excinfo.value)


def test_list_deployments_empty(client):
    """When no deployment has been made yet, the deployment list is empty."""
    deployments = client.list_deployments()
    assert len(deployments) == 0


def test_list_deployments_kubernetes_api_error(client):
    """When a non-specific Kubernetes API error happens on `list`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.list_namespaced_service.side_effect = (
            kubernetes.client.rest.ApiException(status=HTTPStatus.BAD_REQUEST)
        )

        client.list_deployments()

    assert "API error" in str(excinfo.value)


def test_list_deployments_endpoint_arg_not_supported_error(client):
    """When the 'endpoint' argument is used with `list`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.list_deployments(endpoint="http://localhost")

    assert "'endpoint' argument not supported" in str(excinfo.value)


def test_get_deployment_ok(client, deployment):
    """When a named deployment exists, we can `get` its descriptive data."""
    data = client.get_deployment(deployment["name"])
    assert data.get("name") == deployment["name"]
    assert deployment["name"] in data.get("url", "")
    assert data.get("run_id") is not None
    assert data.get("generation") is not None


def test_get_deployment_endpoint_arg_not_supported_error(client):
    """When the 'endpoint' argument is used with `get`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.get_deployment("test", endpoint="http://localhost")

    assert "'endpoint' argument not supported" in str(excinfo.value)


def test_get_deployment_no_match_error(client):
    """When no deployment matches the provided `name`, an error is raised."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.get_deployment("bad-name")

    assert "must return exactly one deployment" in str(excinfo.value)


def test_get_deployment_kubernetes_api_error(client, deployment):
    """When a non-specific Kubernetes API error happens on `get`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.side_effect = (
            kubernetes.client.rest.ApiException(status=HTTPStatus.BAD_REQUEST)
        )

        client.get_deployment(deployment["name"])

    assert "API error" in str(excinfo.value)


def test_create_deployment_ok(client, deployment):
    """Ensure a single named deployment is created."""
    assert "name" in deployment
    assert deployment.get("flavor") == MLFLOW_MODEL_FLAVOR_PYFUNC

    assert len(client.list_deployments()) == 1  # Only one deployment is created


def test_create_deployment_flavor_not_supported_error(
    client, image_repository, first_run
):
    """When an unsupported model flavor is provided to `create`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.create_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            flavor="bad",
        )

    assert "flavor" in str(excinfo.value)


def test_create_deployment_endpoint_arg_not_supported_error(
    client, image_repository, first_run
):
    """When the 'endpoint' argument is used with `create`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.create_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            endpoint="http://localhost",
        )

    assert "'endpoint' argument not supported" in str(excinfo.value)


def test_create_deployment_duplicate_error(
    client, image_repository, deployment, first_run
):
    """When a deployment by the same `name` already exists, `create` raises an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as create_excinfo:
        # Can't create the same deployment twice!
        client.create_deployment(
            name=deployment["name"],
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": first_run.info.run_id,
            },
        )

    assert "already exists" in str(create_excinfo.value)
    assert len(client.list_deployments()) == 1  # Only one deployment still exists


def test_create_deployment_kubernetes_api_error(client, image_repository, first_run):
    """When a non-specific Kubernetes API error happens on `create`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.create_namespaced_service.side_effect = (
            kubernetes.client.rest.ApiException(status=HTTPStatus.BAD_REQUEST)
        )

        client.create_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": first_run.info.run_id,
            },
        )

    assert "API error" in str(excinfo.value)


def test_create_deployment_timeout_error(client, image_repository, first_run):
    """When a timeout happens on `create`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException),
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.create_namespaced_service.side_effect = (
            knative.KnativeTimeoutError
        )

        client.create_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": first_run.info.run_id,
            },
        )


def test_update_deployment_ok(client, image_repository, deployment, second_run):
    """Ensure a single named deployment is updated."""
    initial_deployment_data = client.get_deployment(deployment["name"])
    initial_generation = int(initial_deployment_data["generation"])
    updated_deployment = client.update_deployment(
        name=deployment["name"],
        model_uri=f"runs:/{second_run.info.run_id}/{ARTIFACT_PATH}",
        config={
            "image_repository": image_repository,
            "image_tag": second_run.info.run_id,
        },
    )

    assert updated_deployment.get("name") == deployment["name"]
    assert updated_deployment.get("flavor") == MLFLOW_MODEL_FLAVOR_PYFUNC

    assert len(client.list_deployments()) == 1  # Only one deployment still exists

    updated_deployment_data = client.get_deployment(deployment["name"])
    assert updated_deployment_data["run_id"] != initial_deployment_data["run_id"]
    updated_generation = int(updated_deployment_data["generation"])
    assert updated_generation > initial_generation


def test_update_deployment_no_revision_ok(
    client, image_repository, deployment, first_run
):
    """Ensure a single named deployment is updated (no new Knative revision).

    Note: Knative will not update the `generation` property if a deployement is updated
    with exactly the same configuration as before.
    """
    initial_deployment_data = client.get_deployment(deployment["name"])
    initial_generation = int(initial_deployment_data["generation"])
    updated_deployment = client.update_deployment(
        name=deployment["name"],
        model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
        config={
            "image_repository": image_repository,
            "image_tag": first_run.info.run_id,
        },
    )

    assert updated_deployment.get("name") == deployment["name"]
    assert updated_deployment.get("flavor") == MLFLOW_MODEL_FLAVOR_PYFUNC

    assert len(client.list_deployments()) == 1  # Only one deployment still exists

    updated_deployment_data = client.get_deployment(deployment["name"])
    assert updated_deployment_data["run_id"] == initial_deployment_data["run_id"]
    updated_generation = int(updated_deployment_data["generation"])
    assert updated_generation == initial_generation


def test_update_deployment_flavor_not_supported_error(
    client, image_repository, first_run
):
    """When an unsupported model flavor is provided to `update`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.update_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            flavor="bad",
        )

    assert "flavor" in str(excinfo.value)


def test_update_deployment_endpoint_arg_not_supported_error(
    client, image_repository, first_run
):
    """When the 'endpoint' argument is used with `update`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.update_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            endpoint="http://localhost",
        )

    assert "'endpoint' argument not supported" in str(excinfo.value)


def test_update_deployment_not_found_error(client, image_repository, second_run):
    """When updating a named deployment, raise if the deployement is not found."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.update_deployment(
            name="bad-name",
            model_uri=f"runs:/{second_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": second_run.info.run_id,
            },
        )

    assert "no deployment with name" in str(excinfo.value)


def test_update_deployment_unmanaged_error(client, image_repository, first_run):
    """When attempting to update an unmanaged deployment, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.return_value = {
            "metadata": {
                "labels": {},
                "annotations": {RUN_ID_ANNOTATION: first_run.info.run_id},
            }
        }

        client.update_deployment(
            name="test",
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": first_run.info.run_id,
            },
        )

    assert "not managed" in str(excinfo.value)


def test_update_deployment_kubernetes_api_error(
    client, image_repository, deployment, first_run
):
    """When a non-specific Kubernetes API error happens on `update`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.return_value = {
            "metadata": {
                "labels": {MANAGED_BY_LABEL: DISTRIBUTION_NAME},
                "annotations": {RUN_ID_ANNOTATION: first_run.info.run_id},
            }
        }
        mock_knative_api.update_namespaced_service.side_effect = (
            kubernetes.client.rest.ApiException(status=HTTPStatus.BAD_REQUEST)
        )

        client.update_deployment(
            name=deployment["name"],
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": first_run.info.run_id,
            },
        )

    assert "API error" in str(excinfo.value)


def test_update_deployment_timeout_error(
    client, image_repository, deployment, first_run
):
    """When a timeout happens on `update`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException),
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.return_value = {
            "metadata": {
                "labels": {MANAGED_BY_LABEL: DISTRIBUTION_NAME},
                "annotations": {RUN_ID_ANNOTATION: first_run.info.run_id},
            }
        }
        mock_knative_api.update_namespaced_service.side_effect = (
            knative.KnativeTimeoutError
        )

        client.update_deployment(
            name=deployment["name"],
            model_uri=f"runs:/{first_run.info.run_id}/{ARTIFACT_PATH}",
            config={
                "image_repository": image_repository,
                "image_tag": first_run.info.run_id,
            },
        )


def test_delete_deployment_ok(client, deployment):
    """Ensure a single named deployment is deleted."""
    client.delete_deployment(deployment["name"])
    assert len(client.list_deployments()) == 0  # The deployment is deleted


def test_delete_deployment_endpoint_arg_not_supported_error(client):
    """When the 'endpoint' argument is used with `delete`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.delete_deployment("test", endpoint="http://localhost")

    assert "'endpoint' argument not supported" in str(excinfo.value)


def test_delete_deployment_not_found_error(client):
    """When deleting a named deployment, raise if the deployement is not found."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.delete_deployment("bad-name")

    assert "no deployment with name" in str(excinfo.value)


def test_delete_deployment_unmanaged_error(client):
    """When attempting to delete an unmanaged deployment, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.return_value = {
            "metadata": {
                "labels": {},
                "annotations": {},
            }
        }

        client.delete_deployment("test")

    assert "not managed" in str(excinfo.value)


def test_delete_deployment_kubernetes_api_error(client, deployment):
    """When a non-specific Kubernetes API error happens on `delete`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.return_value = {
            "metadata": {
                "labels": {MANAGED_BY_LABEL: DISTRIBUTION_NAME},
                "annotations": {},
            }
        }
        mock_knative_api.delete_namespaced_service.side_effect = (
            kubernetes.client.rest.ApiException(status=HTTPStatus.BAD_REQUEST)
        )

        client.delete_deployment(deployment["name"])

    assert "API error" in str(excinfo.value)


def test_predict_ok(client, deployment):
    """When using a deployment for predictions, return a list of results."""
    with patch("mlflow_knative.deployment_client.requests") as mock_requests:
        mock_response = mock_requests.post.return_value
        mock_response.json.return_value = []

        results = client.predict(deployment_name=deployment["name"])

    assert results == []


def test_predict_kubernetes_api_error(client):
    """When a non-specific Kubernetes API error happens on `predict`, raise an error."""
    with (
        patch(
            "mlflow_knative.deployment_client.KnativeServingV1Api"
        ) as MockKnativeServingV1Api,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_knative_api = MockKnativeServingV1Api.return_value
        mock_knative_api.get_namespaced_service.side_effect = (
            kubernetes.client.rest.ApiException(status=HTTPStatus.BAD_REQUEST)
        )

        client.predict(deployment_name="test")

    assert "API error" in str(excinfo.value)


def test_predict_bad_request(client, deployment):
    """When using a deployment for predictions, raise on bad request."""
    with (
        patch("mlflow_knative.deployment_client.requests.post") as mock_post,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_post.side_effect = requests.exceptions.HTTPError

        client.predict(deployment_name=deployment["name"])

    assert "deployment request error" in str(excinfo.value)


def test_predict_missing_deployment_name_error(client):
    """When the 'deployment_name' argument is missing, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.predict()

    assert "a deployment name is required" in str(excinfo.value)


def test_predict_endpoint_arg_not_supported_error(client):
    """When the 'endpoint' argument is used with `predict`, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.predict(deployment_name="test", endpoint="http://localhost")

    assert "'endpoint' argument not supported" in str(excinfo.value)


def test_predict_deployment_not_available_error(client):
    """When using a deployemnt on an unavailable domain, raise an error."""
    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        client.get_deployment = MagicMock()
        client.get_deployment.return_value = {"url": "http://unavailable.domain.local"}
        client.predict(deployment_name="test")

    assert "could not connect to the deployment" in str(excinfo.value)
