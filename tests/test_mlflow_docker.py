import sys
from unittest.mock import MagicMock, patch

import docker
import mlflow
import pytest

from mlflow_knative import mlflow_docker

from .conftest import ARTIFACT_PATH


def test_get_model_as_image_registry_error(image_repository, run):
    """When the docker client fails to get registry data, raise an error."""
    client = MagicMock()
    client.images.get_registry_data.side_effect = docker.errors.APIError(
        "failed to get registry data (this is a test)"
    )

    with pytest.raises(mlflow.exceptions.MlflowException) as excinfo:
        mlflow_docker.get_model_as_image(
            client, f"runs:/{run.info.run_id}/{ARTIFACT_PATH}", image_repository
        )

    assert "failed to get registry data" in str(excinfo)


def test_get_model_as_image_build_error(image_repository, run):
    """When the MLflow-managed docker build fails, raise an error."""
    build_error_message = "ERROR: mock failure of test build"

    with (
        patch("mlflow.models.build_docker") as mock_build_docker,
        pytest.raises(mlflow.exceptions.MlflowException) as excinfo,
    ):
        mock_build_docker.side_effect = lambda **_: print(
            build_error_message, file=sys.stderr
        )
        mlflow_docker.get_model_as_image(
            MagicMock(),
            f"runs:/{run.info.run_id}/{ARTIFACT_PATH}",
            image_repository,
            force_update=True,
        )

    assert build_error_message in str(excinfo)


@patch("mlflow.models.build_docker")
def test_get_model_as_image_push_error(image_repository, run):
    """On failing to push the image, raise an error."""
    client = MagicMock()
    client.images.push.return_value = '{"error": "mock push error"}'

    with (pytest.raises(mlflow.exceptions.MlflowException) as excinfo,):
        mlflow_docker.get_model_as_image(
            client,
            f"runs:/{run.info.run_id}/{ARTIFACT_PATH}",
            image_repository,
            force_update=True,
        )

    assert "failed to push image" in str(excinfo)
