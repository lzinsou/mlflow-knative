"""Knative service body templating testing."""
from mlflow_knative.templating import get_service_body


def test_get_service_body():
    """Ensure the templated body is rendered."""
    body = get_service_body(
        namespace="world",
        name="hello",
        image="ghcr.io/knative/helloworld-go:latest",
        template_path="tests/service.yaml",
    )

    assert body == {
        "apiVersion": "serving.knative.dev/v1",
        "kind": "Service",
        "metadata": {"name": "hello", "namespace": "world"},
        "spec": {
            "template": {
                "spec": {
                    "containers": [
                        {
                            "env": [{"name": "TARGET", "value": "World"}],
                            "image": "ghcr.io/knative/helloworld-go:latest",
                            "ports": [{"containerPort": 8080}],
                        }
                    ]
                }
            }
        },
    }
