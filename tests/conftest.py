"""Pytest configuration, including extra arguments."""
import mlflow
import pytest
from sklearn.ensemble import RandomForestClassifier

ARG_CONTEXT = "--context"
ARG_IMAGE_REPOSITORY = "--image-repository"

ARTIFACT_PATH = "model"


def pytest_addoption(parser):
    """Add a '--context' option to `pytest` to set a Kubernetes client context."""
    parser.addoption(
        ARG_CONTEXT,
        action="store",
        default="kind-kind",
        help="context for the Kubernetes client",
    )
    parser.addoption(
        ARG_IMAGE_REPOSITORY,
        action="store",
        default="test-model",
        help="test model image repository",
    )


@pytest.fixture(scope="session")
def context(request):
    """Get the Kubernetes client context."""
    return request.config.getoption(ARG_CONTEXT)


@pytest.fixture(scope="session")
def image_repository(request):
    """Get the test model image repository."""
    return request.config.getoption(ARG_IMAGE_REPOSITORY)


@pytest.fixture(scope="session")
def run():
    with mlflow.start_run() as run:
        mlflow.sklearn.log_model(RandomForestClassifier(), ARTIFACT_PATH)

    return run


# First run ID and second run ID are different from each other, but constant across the
# test session so the deployment client will use caching when possible.
first_run = second_run = run
